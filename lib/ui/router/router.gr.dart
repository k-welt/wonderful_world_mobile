// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

import '../../data/model/user/user.dart';
import '../../data/model/world/world.dart';
import '../screen/create_world/createworld_screen.dart';
import '../screen/current_user/currentuser_screen.dart';
import '../screen/home/home_screen.dart';
import '../screen/login/login_screen.dart';
import '../screen/login_cwelt/logincwelt_screen.dart';
import '../screen/login_vk/loginvk_screen.dart';
import '../screen/reflection/reflection_screen.dart';
import '../screen/search_worlds/searchworlds_screen.dart';
import '../screen/start/start_screen.dart';
import '../screen/user/user_screen.dart';
import '../screen/world/world_screen.dart';

class Routes {
  static const String startScreen = '/';
  static const String homeScreen = '/home-screen';
  static const String loginCWeltScreen = '/login-cwelt-screen';
  static const String loginVkScreen = '/login-vk-screen';
  static const String loginScreen = '/login-screen';
  static const String createworldScreen = '/createworld-screen';
  static const String currentuserScreen = '/currentuser-screen';
  static const String searchworldsScreen = '/searchworlds-screen';
  static const String worldScreen = '/world-screen';
  static const String userScreen = '/user-screen';
  static const String reflectionScreen = '/reflection-screen';
  static const all = <String>{
    startScreen,
    homeScreen,
    loginCWeltScreen,
    loginVkScreen,
    loginScreen,
    createworldScreen,
    currentuserScreen,
    searchworldsScreen,
    worldScreen,
    userScreen,
    reflectionScreen,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startScreen, page: StartScreen),
    RouteDef(Routes.homeScreen, page: HomeScreen),
    RouteDef(Routes.loginCWeltScreen, page: LoginCWeltScreen),
    RouteDef(Routes.loginVkScreen, page: LoginVkScreen),
    RouteDef(Routes.loginScreen, page: LoginScreen),
    RouteDef(Routes.createworldScreen, page: CreateworldScreen),
    RouteDef(Routes.currentuserScreen, page: CurrentuserScreen),
    RouteDef(Routes.searchworldsScreen, page: SearchworldsScreen),
    RouteDef(Routes.worldScreen, page: WorldScreen),
    RouteDef(Routes.userScreen, page: UserScreen),
    RouteDef(Routes.reflectionScreen, page: ReflectionScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    StartScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => StartScreen(),
        settings: data,
      );
    },
    HomeScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => HomeScreen(),
        settings: data,
      );
    },
    LoginCWeltScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginCWeltScreen(),
        settings: data,
      );
    },
    LoginVkScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginVkScreen(),
        settings: data,
      );
    },
    LoginScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => LoginScreen(),
        settings: data,
      );
    },
    CreateworldScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CreateworldScreen(),
        settings: data,
      );
    },
    CurrentuserScreen: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => CurrentuserScreen(),
        settings: data,
      );
    },
    SearchworldsScreen: (data) {
      final args = data.getArgs<SearchworldsScreenArguments>(
        orElse: () => SearchworldsScreenArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => SearchworldsScreen(
          key: args.key,
          name: args.name,
          authorName: args.authorName,
          authorId: args.authorId,
        ),
        settings: data,
      );
    },
    WorldScreen: (data) {
      final args = data.getArgs<WorldScreenArguments>(
        orElse: () => WorldScreenArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => WorldScreen(
          key: args.key,
          world: args.world,
        ),
        settings: data,
      );
    },
    UserScreen: (data) {
      final args = data.getArgs<UserScreenArguments>(
        orElse: () => UserScreenArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => UserScreen(
          key: args.key,
          user: args.user,
        ),
        settings: data,
      );
    },
    ReflectionScreen: (data) {
      final args = data.getArgs<ReflectionScreenArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => ReflectionScreen(
          key: args.key,
          id: args.id,
          ownerId: args.ownerId,
          type: args.type,
          worldWidth: args.worldWidth,
          worldHeight: args.worldHeight,
          isLast: args.isLast,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// SearchworldsScreen arguments holder class
class SearchworldsScreenArguments {
  final Key key;
  final String name;
  final String authorName;
  final String authorId;
  SearchworldsScreenArguments(
      {this.key, this.name, this.authorName, this.authorId});
}

/// WorldScreen arguments holder class
class WorldScreenArguments {
  final Key key;
  final World world;
  WorldScreenArguments({this.key, this.world});
}

/// UserScreen arguments holder class
class UserScreenArguments {
  final Key key;
  final User user;
  UserScreenArguments({this.key, this.user});
}

/// ReflectionScreen arguments holder class
class ReflectionScreenArguments {
  final Key key;
  final String id;
  final String ownerId;
  final int type;
  final int worldWidth;
  final int worldHeight;
  final bool isLast;
  ReflectionScreenArguments(
      {this.key,
      @required this.id,
      @required this.ownerId,
      @required this.type,
      @required this.worldWidth,
      @required this.worldHeight,
      this.isLast = false});
}

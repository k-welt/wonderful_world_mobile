import 'package:flutter/material.dart';

class SingleChildScrollBarView extends StatefulWidget {
  final Widget child;
  final ScrollController controller;
  final Axis scrollDirection;

  const SingleChildScrollBarView({
    Key key,
    this.controller,
    this.scrollDirection = Axis.vertical,
    @required this.child,
  }) : super(key: key);

  @override
  _SingleChildScrollBarViewState createState() =>
      _SingleChildScrollBarViewState();
}

class _SingleChildScrollBarViewState extends State<SingleChildScrollBarView> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _controller,
      child: SingleChildScrollView(
        controller: _controller,
        scrollDirection: widget.scrollDirection,
        child: widget.child,
      ),
    );
  }
}

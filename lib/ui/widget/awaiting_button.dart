import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AwaitingButton<T extends Cubit<S>, S> extends StatelessWidget {
  final String title;
  final VoidCallback onPressed;
  final bool Function(S state) isEnabledDelegat;
  final bool Function(S state) isAwaitDelegat;

  const AwaitingButton({
    Key key,
    @required this.title,
    @required this.onPressed,
    @required this.isEnabledDelegat,
    @required this.isAwaitDelegat,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<T, S>(
      builder: (context, state) {
        if (isEnabledDelegat(state)) {
          return RaisedButton(
            child: Text(title),
            onPressed: onPressed,
          );
        } else if (isAwaitDelegat(state)) {
          return RaisedButton(
            child: CircularProgressIndicator(),
            disabledColor: Theme.of(context).scaffoldBackgroundColor,
            onPressed: null,
          );
        } else {
          return RaisedButton(
            child: Text(title),
            onPressed: null,
          );
        }
      },
    );
  }
}

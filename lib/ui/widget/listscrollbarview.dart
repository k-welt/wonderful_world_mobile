import 'package:flutter/material.dart';

class ListScrollBarView extends StatefulWidget {
  final List<Widget> children;
  final IndexedWidgetBuilder itemBuilder;
  final int itemCount;
  final ScrollController controller;
  final Axis scrollDirection;
  final EdgeInsetsGeometry padding;
  final bool _isBuilder;

  const ListScrollBarView({
    Key key,
    this.controller,
    this.scrollDirection = Axis.vertical,
    this.padding,
    @required this.children,
  })  : _isBuilder = false,
        itemBuilder = null,
        itemCount = 0,
        super(key: key);

  const ListScrollBarView.builder({
    Key key,
    this.controller,
    this.scrollDirection = Axis.vertical,
    this.padding,
    @required this.itemBuilder,
    @required this.itemCount,
  })  : _isBuilder = true,
        children = null,
        super(key: key);

  @override
  State<ListScrollBarView> createState() => (_isBuilder)
      ? _ListBuilderScrollBarViewState()
      : _ListScrollBarViewState();
}

class _ListScrollBarViewState extends State<ListScrollBarView> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _controller,
      child: ListView(
        controller: _controller,
        padding: widget.padding,
        scrollDirection: widget.scrollDirection,
        children: widget.children,
      ),
    );
  }
}

class _ListBuilderScrollBarViewState extends State<ListScrollBarView> {
  ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _controller = widget.controller ?? ScrollController();
  }

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      controller: _controller,
      child: ListView.builder(
        controller: _controller,
        padding: widget.padding,
        scrollDirection: widget.scrollDirection,
        itemCount: widget.itemCount,
        itemBuilder: widget.itemBuilder,
      ),
    );
  }
}

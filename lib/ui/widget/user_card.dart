import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/widget/listitem.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';

import 'listscrollbarview.dart';

class UserCard extends StatefulWidget {
  static int numberOfImplementations = 0;
  final EdgeInsets padding;
  final User user;
  final bool isCurrent;

  const UserCard({
    Key key,
    this.padding = Indents.kDefaultBottomIndention,
    @required this.user,
    this.isCurrent = false,
  }) : super(key: key);

  @override
  _UserCardState createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {
  @override
  void initState() {
    super.initState();
    UserCard.numberOfImplementations++;
  }

  @override
  void dispose() {
    super.dispose();
    UserCard.numberOfImplementations--;
  }

  @override
  Widget build(BuildContext context) {
    var l10n = S.of(context);
    return ListScrollBarView(
        padding: Indents.kDefaultBottomIndention,
        children: [
          ListItem(
              title: l10n.userWidget_card_id,
              description: widget.user.id.toString()),
          ListItem(
              title: l10n.userWidget_card_username,
              description: widget.user.username),
          if (widget.isCurrent)
            ListItem(
                title: l10n.userWidget_card_email,
                description: widget.user.email),
          ListItem(
            title: l10n
                .userWidget_card_worldsCreated(widget.user.createdWorldCount),
            description: (widget.user.createdWorldCount > 0)
                ? l10n.userWidget_card_worldsCreated_button
                : null,
            trailing: Icon(Icons.search),
            onDescriptionPressed: (widget.user.createdWorldCount > 0)
                ? () => worldsListPressedListener(context)
                : null,
          ),
        ],
    );
  }

  void worldsListPressedListener(BuildContext context) {
    ExtendedNavigator.root.push(
      Routes.searchworldsScreen,
      arguments: SearchworldsScreenArguments(authorId: widget.user.id),
    );
  }
}

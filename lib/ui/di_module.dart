import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'cubit/theme_cubit.dart';
import 'screen/di_module.dart';
import 'widget/di_module.dart';

var uiModules = [
  ...screenModules,
  widgetModule,
  Module()..cubit((scope) => ThemeCubit(scope.get())),
];

import 'package:flutter/material.dart' hide Router;
import 'package:auto_route/auto_route.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';
import 'cubit/theme_cubit.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with ScopeStateMixin {
  ThemeCubit _cubit;
  @override
  void initState() {
    super.initState();
    _cubit = get();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ThemeCubit, ThemeState>(
      cubit: _cubit,
      builder: (context, state) {
        return MaterialApp(
          localizationsDelegates: [
            S.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: S.delegate.supportedLocales,
          themeMode: state.isDark ? ThemeMode.dark : ThemeMode.system,
          theme: ThemeData(brightness: Brightness.light),
          darkTheme: ThemeData(brightness: Brightness.dark),
          builder: (context, nativeNavigator) {
            return ExtendedNavigator<Router>(router: Router());
          },
        );
      },
    );
  }
}

import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'package:wonderful_world/ui/screen/home/component/body/cubit/available_worlds_body_cubit.dart';
import 'component/body/cubit/created_worlds_body_cubit.dart';
import 'component/body/cubit/selected_worlds_body_cubit.dart';
import 'cubit/homescreen_cubit.dart';

var homescreenModule = Module()
  ..cubit((scope) => HomescreenCubit(scope.get()))
  ..cubit((scope) => AvailableworldsbodyCubit(scope.get()))
  ..cubit((scope) => SelectedworldsbodyCubit(scope.get()))
  ..cubit((scope) => CreatedworldsbodyCubit(scope.get()));

import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/ui/widget/worldlist/cubit/worldlist_cubit.dart';

class CreatedworldsbodyCubit extends WorldlistCubit {
  CreatedworldsbodyCubit(
    WorldCenter worldCenter,
  ) : super(worldCenter);

  @override
  Future<List<World>> getWorlds() async {
    return await worldCenter.createdWorlds();
  }
}

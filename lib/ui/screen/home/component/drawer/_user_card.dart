part of 'drawer_component.dart';

class _UserCard extends StatelessWidget with KoinComponentMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: Divider.createBorderSide(context))),
      child: Container(
        margin:
            Indents.kDefaultLeftIndention + Indents.kDefaultVerticalIndention,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.horizontal(
              left: const Radius.circular(10.0),
            ),
            color: Theme.of(context).cardColor),
        child: BlocBuilder<HomescreenCubit, HomescreenState>(
          buildWhen: (previous, current) => previous.isAuth != current.isAuth,
          builder: _blocBuilder,
        ),
      ),
    );
  }

  Widget _blocBuilder(BuildContext context, HomescreenState state) {
    var l10n = S.of(context);
    if (state.isAuth) {
      return ListTile(
          onTap: _cardPressedListener,
          leading: Icon(
            Icons.account_circle,
            size: 50.0,
          ),
          title: Text(state.username),
          subtitle: Text(l10n.homeScreen_drawer_userCard_button_auth));
    } else {
      return ListTile(
          onTap: _cardPressedListener,
          leading: Icon(
            Icons.account_circle,
            size: 50.0,
          ),
          title: Text(l10n.homeScreen_drawer_userCard_title_default),
          subtitle: Text(l10n.homeScreen_drawer_userCard_button_unauth));
    }
  }

  void _cardPressedListener() {
    get<HomescreenCubit>().userCardPressedListener();
  }
}

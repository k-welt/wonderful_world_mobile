import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin/koin.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/cubit/theme_cubit.dart';
import 'package:wonderful_world/ui/screen/home/cubit/homescreen_cubit.dart';

part '_user_card.dart';
part '_theme_switcher.dart';

class DrawerComponent extends StatefulWidget {
  final List<LinkItem> items;

  const DrawerComponent({Key key, @required this.items}) : super(key: key);

  @override
  _DrawerComponentState createState() => _DrawerComponentState();
}

class _DrawerComponentState extends State<DrawerComponent>
    with ScopeStateMixin {
  MediaQueryData _mediaQuery;
  HomescreenCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _mediaQuery = MediaQuery.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: EdgeInsets.only(top: _mediaQuery.padding.top),
        child: Column(
          children: [
            _UserCard(),
            Expanded(
              child: ListView(
                children: [
                  for (var iter in widget.items)
                    Container(
                      margin: Indents.kDefaultLeftIndention,
                      child: RaisedButton(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (_isItemDisable(iter)) ...[
                              Text(iter.title),
                            ] else ...[
                              Text(iter.title),
                              Icon(Icons.keyboard_arrow_right),
                            ]
                          ],
                        ),
                        onPressed: _isItemDisable(iter)
                            ? null
                            : () => _itemPressedListener(context, iter.index),
                      ),
                    )
                ],
              ),
            ),
            _ThemeSwitcher(),
          ],
        ),
      ),
    );
  }

  void _itemPressedListener(BuildContext context, int index) {
    _cubit.draverLinkTapListener(index);
  }

  bool _isItemDisable(LinkItem item) {
    var result;
    result = item.index == _cubit.state.index;
    result = result || item.isLimitedAccess && !_cubit.state.isAuth;

    return result;
  }
}

class LinkItem {
  final int index;
  final String title;
  final bool isLimitedAccess;

  LinkItem(this.index, this.title, this.isLimitedAccess);
}

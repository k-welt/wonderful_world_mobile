import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/screen/reflection/component/body/gol_component.dart';
import 'package:wonderful_world/ui/widget/awaiting.dart';

import 'component/bottombar/bottombar_component.dart';
import 'component/appbar_component.dart';
import 'component/zoom_component.dart';
import 'cubit/reflectionscreen_cubit.dart';

class ReflectionScreen extends StatefulWidget {
  final String id;
  final String ownerId;
  final int type;
  final int worldWidth;
  final int worldHeight;
  final bool isLast;

  const ReflectionScreen({
    Key key,
    @required this.id,
    @required this.ownerId,
    @required this.type,
    @required this.worldWidth,
    @required this.worldHeight,
    this.isLast = false,
  }) : super(key: key);

  @override
  _ReflectionScreenState createState() => _ReflectionScreenState();
}

class _ReflectionScreenState extends State<ReflectionScreen>
    with ScopeStateMixin {
  ReflectionscreenCubit _cubit;
  S _l10n;
  List<Widget> _panelItemTitles;
  ButtonThemeData _buttonThemeData;
  DividerThemeData _dividerThemeData;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
    if (widget.isLast) {
      _cubit.lastDataLoad();
    } else {
      _cubit.load(widget.id, widget.ownerId);
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
    _panelItemTitles = GOLComponent.getPanelItemTitles(context)
        .map(
          (value) => Text(value),
        )
        .toList();
    _buttonThemeData = ButtonTheme.of(context);
    _dividerThemeData = DividerTheme.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: _cubit,
      child: BlocBuilder<ReflectionscreenCubit, ReflectionscreenState>(
        buildWhen: (previous, current) =>
            previous is ReflectionscreenInitial ||
            previous.elementColors != current.elementColors ||
            previous.elementColors.background !=
                current.elementColors.background ||
            previous.elementColors.greed != current.elementColors.greed ||
            previous.elementColors.cells != current.elementColors.cells ||
            previous.elementColors.cells[0] != current.elementColors.cells[0],
        builder: (context, state) {
          if (state is ReflectionscreenInitial ||
              state is ReflectionscreenLoadingFail) {
            return _loadingStateBodyBuilder(context, state);
          } else {
            return Scaffold(
              appBar: AppBarComponent(),
              body: ZoomComponent(
                margin: EdgeInsets.only(
                  bottom: _buttonThemeData.height +
                      (_dividerThemeData?.thickness ?? 16.0),
                ),
                type: widget.type,
                worldWidth: widget.worldWidth,
                worldHeight: widget.worldHeight,
              ),
              bottomSheet: Container(
                decoration: BoxDecoration(
                    border: Border(top: Divider.createBorderSide(context))),
                child: BottombarComponent(
                  itemTitles: _panelItemTitles,
                ),
              ),
            );
          }
        },
      ),
    );
  }

  Widget _loadingStateBodyBuilder(BuildContext _, ReflectionscreenState state) {
    Widget body;
    if (state is ReflectionscreenInitial) {
      body = Awaiting();
    } else {
      body = Align(
        alignment: const Alignment(0.0, 0.50),
        child: Card(
          child: ListTile(
            title: Container(
              margin: Indents.kDefaultBottomIndention,
              child: Text(
                _l10n.reflectionScreen_card_loadingFail,
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ),
        ),
      );
    }
    return Scaffold(
      appBar: AppBar(title: Text(_l10n.reflectionScreen_title_loading)),
      body: body,
      bottomNavigationBar: Padding(
        padding: Indents.kDefaultHorizontalIndention,
        child: FlatButton(
          child: Text(_l10n.button_cancel),
          onPressed: _cubit.cancelPressedListener,
        ),
      ),
    );
  }
}

class PanelItemColors {
  Color background;
  Color greed;
  List<Color> cells;

  PanelItemColors({this.background, this.greed, this.cells});
}

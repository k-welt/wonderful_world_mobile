import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';

import 'cubit/reflectionbodycomponent_cubit.dart';
import 'gol_component.dart';

var reflectionbodycomponentModule = Module()
  ..scopeOneCubit<ReflectionbodycomponentCubit, GOLComponent>(
    (scope) => ReflectionbodycomponentCubit(scope.get()),
  );

import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';
import 'package:wonderful_world/domain/warp_reflection/warpreflection_center.dart';

part 'reflectionbodycomponent_state.dart';

class ReflectionbodycomponentCubit extends Cubit<ReflectionbodycomponentState> {
  final WarpReflectionCenter _warpCenter;
  StreamSubscription _reflectionSubscription;
  ReflectionbodycomponentCubit(this._warpCenter)
      : super(ReflectionbodycomponentInitial()) {
    _reflectionSubscription = _warpCenter.reflectionObservable.listen(reflect);
    _warpCenter.getReflection();
  }

  void reflect(List<ReflectItem> items) {
    emit(ReflectionbodycomponentShow(items));
  }

  void remove(Point<int> point) {
    _warpCenter.remove(point);
    _warpCenter.getReflection();
  }

  void insert(Point<int> point, int editType) {
    _warpCenter.insert(point);
    _warpCenter.getReflection();
  }

  @override
  Future<void> close() async {
    _reflectionSubscription.cancel();
    await super.close();
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/screen/reflection/cubit/reflectionscreen_cubit.dart';
import 'package:wonderful_world/ui/widget/singlechildscrollbarview.dart';

class BottombarComponent extends StatefulWidget {
  final List<Widget> itemTitles;

  const BottombarComponent({
    Key key,
    @required this.itemTitles,
  })  : assert(itemTitles != null && itemTitles.length > 0),
        super(key: key);

  @override
  _BottombarComponentState createState() => _BottombarComponentState();
}

class _BottombarComponentState extends State<BottombarComponent> {
  S _l10n;
  MediaQueryData _mediaQuery;
  ReflectionscreenCubit _cubit;

  @override
  void initState() {
    super.initState();
    _cubit = BlocProvider.of<ReflectionscreenCubit>(context);
  }

  @override
  void didChangeDependencies() {
    _l10n = S.of(context);
    _mediaQuery = MediaQuery.of(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ReflectionscreenCubit, ReflectionscreenState>(
      builder: (context, state) {
        return Container(
          margin: Indents.kDefaultHorizontalIndention,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Expanded(
                    child: FlatButton(
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(_l10n.reflectionScreen_button_options),
                          Icon(
                            (state.isBottombarOpen)
                                ? Icons.keyboard_arrow_down
                                : Icons.keyboard_arrow_up,
                          )
                        ],
                      ),
                      onPressed: _cubit.bottombarPressedListener,
                    ),
                  ),
                  FlatButton(
                    child: Text(
                      (state.isPlayed)
                          ? _l10n.reflectionScreen_button_stop
                          : _l10n.reflectionScreen_button_play,
                    ),
                    onPressed: _cubit.playpausePressedListener,
                  )
                ],
              ),
              AnimatedContainer(
                constraints: BoxConstraints(
                    maxHeight: (state.isBottombarOpen)
                        ? _mediaQuery.size.height / 2
                        : 0),
                child: SingleChildScrollBarView(
                  child: Column(
                    children: [
                      Slider(
                        min: 1,
                        max: 500,
                        value: state.jiffMillisecDuration,
                        onChanged: _cubit.setJiffMillisecDurationListener,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                if (state is ReflectionscreenBrowsing) ...[
                                  FlatButton(
                                    onPressed: () =>
                                        _cubit.backgroundcolorPressedListener(
                                            context),
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.circle,
                                          color: state.elementColors.background,
                                        ),
                                        const VerticalDivider(),
                                        Text(_l10n
                                            .reflectionScreen_panel_browsing_item_background)
                                      ],
                                    ),
                                  ),
                                  FlatButton(
                                    onPressed: () => _cubit
                                        .greedcolorPressedListener(context),
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.circle,
                                          color: state.elementColors.greed,
                                        ),
                                        const VerticalDivider(),
                                        Text(_l10n
                                            .reflectionScreen_panel_browsing_item_grid)
                                      ],
                                    ),
                                  ),
                                  for (int i = 0;
                                      i < widget.itemTitles.length;
                                      ++i)
                                    FlatButton(
                                      onPressed: () => _cubit
                                          .itemcolorPressedListener(context, i),
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.circle,
                                            color: state.elementColors.cells[i],
                                          ),
                                          const VerticalDivider(),
                                          widget.itemTitles[i]
                                        ],
                                      ),
                                    ),
                                  FlatButton(
                                    onPressed: _cubit.resetcolorPressedListener,
                                    child: Row(
                                      children: [
                                        Icon(
                                          Icons.replay_sharp,
                                        ),
                                        const VerticalDivider(),
                                        Text(_l10n
                                            .reflectionScreen_panel_browsing_item_reset)
                                      ],
                                    ),
                                  ),
                                ] else ...[
                                  FlatButton(
                                    onPressed: () =>
                                        _cubit.editTypeChangeListener(0),
                                    shape: (state.editType == 0)
                                        ? RoundedRectangleBorder(
                                            side: BorderSide(
                                                color: Theme.of(context)
                                                    .buttonColor),
                                            borderRadius:
                                                BorderRadius.circular(10))
                                        : null,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.cleaning_services,
                                          color: Colors.redAccent,
                                        ),
                                        const VerticalDivider(),
                                        Text(_l10n
                                            .reflectionScreen_panel_editing_item_remove),
                                      ],
                                    ),
                                  ),
                                  for (int i = 0;
                                      i < widget.itemTitles.length;
                                      ++i)
                                    FlatButton(
                                      onPressed: () =>
                                          _cubit.editTypeChangeListener(i + 1),
                                      shape: (state.editType == i + 1)
                                          ? RoundedRectangleBorder(
                                              side: BorderSide(
                                                  color: Theme.of(context)
                                                      .buttonColor),
                                              borderRadius:
                                                  BorderRadius.circular(10))
                                          : null,
                                      child: Row(
                                        children: [
                                          Icon(
                                            Icons.circle,
                                            color: state.elementColors.cells[i],
                                          ),
                                          const VerticalDivider(),
                                          widget.itemTitles[i]
                                        ],
                                      ),
                                    ),
                                ],
                              ],
                            ),
                          ),
                          FlatButton(
                            child: Text(
                              (state is ReflectionscreenBrowsing)
                                  ? _l10n.reflectionScreen_title_editing
                                  : _l10n.reflectionScreen_title_browsing,
                            ),
                            onPressed: _cubit.swapPressedListener,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                duration: kTabScrollDuration,
                curve: Curves.linear,
              ),
            ],
          ),
        );
      },
    );
  }
}

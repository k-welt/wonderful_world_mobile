import 'package:flutter/material.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/ui/widget/user_card.dart';

class UserScreen extends StatefulWidget {
  final User user;
  const UserScreen({Key key, this.user}) : super(key: key);

  @override
  _UserScreenState createState() => _UserScreenState();
}

class _UserScreenState extends State<UserScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.user.username),
      ),
      body: UserCard(
        user: widget.user,
      ),
    );
  }
}
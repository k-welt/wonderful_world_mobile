import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/screen/current_user/cubit/currentuser_cubit.dart';
import 'package:wonderful_world/ui/widget/awaiting.dart';
import 'package:wonderful_world/ui/widget/buttons_bar.dart';
import 'package:wonderful_world/ui/widget/user_card.dart';

class CurrentuserScreen extends StatefulWidget {
  @override
  _CurrentuserScreenState createState() => _CurrentuserScreenState();
}

class _CurrentuserScreenState extends State<CurrentuserScreen>
    with ScopeStateMixin {
  CurrentuserscreenCubit _cubit;
  S _l10n;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CurrentuserscreenCubit, CurrentuserscreenState>(
        cubit: _cubit,
        builder: (context, state) {
          return Scaffold(
            appBar: AppBar(
              title: Text(_l10n.currentUserScreen_title),
            ),
            bottomNavigationBar: ButtonsBar(
              actionTitle: _l10n.currentUserScreen_button_logout,
              actionPressed: _cubit.logout,
            ),
            body: (state is CurrentuserscreenInitial)
                ? Awaiting()
                : RefreshIndicator(
                    child: UserCard(
                      user: state.user,
                      isCurrent: true,
                    ),
                    onRefresh: () async => _cubit.update(),
                  ),
          );
        });
  }
}

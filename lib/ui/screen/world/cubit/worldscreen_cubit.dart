import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';
import 'package:wonderful_world/ui/widget/user_card.dart';

part 'worldscreen_state.dart';

class WorldscreenCubit extends Cubit<WorldscreenState> {
  final UserCenter _userCenter;
  World world;
  WorldscreenCubit(this._userCenter) : super(WorldscreenInitial());

  Future<void> load(World world) async {
    this.world = world;
    if (UserCard.numberOfImplementations < 1) {
      _userCenter.user(world.authorId).then((user) {
        emit(WorldscreenLoaded(user));
      });
    }
  }

  void ownerPressedListener() {
    ExtendedNavigator.root.push(
      Routes.userScreen,
      arguments: UserScreenArguments(user: state.user),
    );
  }

  void openPressedListener() {
    ExtendedNavigator.root.push(
      Routes.reflectionScreen,
      arguments: ReflectionScreenArguments(
        id: world.id,
        ownerId: world.authorId,
        type: world.type,
        worldWidth: world.width,
        worldHeight: world.height,
      ),
    );
  }
}

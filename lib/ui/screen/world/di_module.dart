import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'cubit/worldscreen_cubit.dart';
import 'world_screen.dart';

var worldModule = Module()
  ..scopeOneCubit<WorldscreenCubit, WorldScreen>(
    (scope) => WorldscreenCubit(scope.get()),
  );

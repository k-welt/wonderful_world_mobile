import 'package:flutter/material.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/widget/popup_menu.dart';
import 'package:wonderful_world/ui/widget/worldlist/worldlist.dart';

import 'cubit/searchworldsscreen_cubit.dart';
import 'component/searchbar_component.dart';

class SearchworldsScreen extends StatefulWidget {
  final String name;
  final String authorName;
  final String authorId;

  const SearchworldsScreen({
    Key key,
    this.name,
    this.authorName,
    this.authorId,
  }) : super(key: key);

  @override
  _SearchworldsScreenState createState() => _SearchworldsScreenState();
}

class _SearchworldsScreenState extends State<SearchworldsScreen>
    with ScopeStateMixin {
  SearchworldsscreenCubit _cubit;
  S _l10n;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
    _cubit.param = SearchParameter(
      name: widget.name,
      authorId: widget.authorId,
      authorName: widget.authorName,
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_l10n.searchWorldsScreen_title),
      ),
      body: WorldList<SearchworldsscreenCubit>(
        head: SearchbarComponent(cubit: _cubit),
        cubit: _cubit,
        contextMenuButtons: ButtonType.select,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/constants/textinput_formatters.dart';
import 'package:wonderful_world/ui/screen/search_worlds/cubit/searchworldsscreen_cubit.dart';
import 'package:wonderful_world/ui/widget/singlechildscrollbarview.dart';

class SearchbarComponent extends StatefulWidget {
  final SearchworldsscreenCubit cubit;

  const SearchbarComponent({Key key, @required this.cubit}) : super(key: key);

  @override
  _SearchbarComponentState createState() => _SearchbarComponentState();
}

class _SearchbarComponentState extends State<SearchbarComponent> {
  TextEditingController _nameController;
  TextEditingController _authorNameController;
  TextEditingController _authorIdController;
  
  S _l10n;
  ButtonThemeData _buttonThemeData;
  ThemeData _theme;
  double _maxHeight;

  List<_SearchLabel> _searchLabels;

  bool isOpen = false;

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController();
    _authorNameController = TextEditingController();
    _authorIdController = TextEditingController();

    _nameController.text = widget.cubit.param?.name;
    _authorNameController.text = widget.cubit.param?.authorName;
    _authorIdController.text = widget.cubit.param?.authorId?.toString();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
    _buttonThemeData = ButtonTheme.of(context);
    _theme = Theme.of(context);
    _maxHeight = MediaQuery.of(context).size.height * 3 / 4;
    _searchLabels = [
      _SearchLabel(title: _l10n.searchWorldsScreen_label_name),
      _SearchLabel(title: _l10n.searchWorldsScreen_label_authorName),
      _SearchLabel(title: _l10n.searchWorldsScreen_label_authorId),
    ];
  }

  @override
  void dispose() {
    _nameController.dispose();
    _authorNameController.dispose();
    _authorIdController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: barTapListener,
      child: Container(
        decoration: BoxDecoration(
          color: _theme.cardColor,
        ),
        child: Column(
          children: [
            Container(
              height: _buttonThemeData.height,
              decoration: BoxDecoration(
                border: Border(
                  bottom: Divider.createBorderSide(context, width: 2.0),
                ),
              ),
              child: Padding(
                padding: Indents.kDefaultHorizontalIndention,
                child: Row(
                  children: [
                    Icon(Icons.search),
                    if (widget.cubit.param?.name != null) _searchLabels[0],
                    if (widget.cubit.param?.authorName != null) _searchLabels[1],
                    if (widget.cubit.param?.authorId != null) _searchLabels[2],
                    Spacer(),
                    IconButton(
                      icon: Icon(Icons.clear),
                      onPressed: clearPressedListener,
                    ),
                  ],
                ),
              ),
            ),
            AnimatedContainer(
              duration: kTabScrollDuration,
              constraints: BoxConstraints(maxHeight: isOpen ? _maxHeight : 0.0),
              child: SingleChildScrollBarView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      // World name
                      padding: Indents.kDefaultHorizontalIndention,
                      child: TextField(
                        controller: _nameController,
                        inputFormatters: TextInputFormatters.fBaseName,
                        decoration: InputDecoration(
                          hintText: _l10n.searchWorldsScreen_label_name,
                          labelText: _l10n.searchWorldsScreen_label_name,
                          suffixIcon: IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: nameClearPressedListener,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      // Author name
                      padding: Indents.kDefaultHorizontalIndention,
                      child: TextField(
                        controller: _authorNameController,
                        inputFormatters: TextInputFormatters.fSearchBaseName,
                        decoration: InputDecoration(
                          hintText: _l10n.searchWorldsScreen_label_authorName,
                          labelText: _l10n.searchWorldsScreen_label_authorName,
                          suffixIcon: IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: authorNameClearPressedListener,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      // Author id
                      padding: Indents.kDefaultHorizontalIndention +
                          Indents.kDefaultBottomIndention,
                      child: TextField(
                        controller: _authorIdController,
                        keyboardType: TextInputType.number,
                        inputFormatters: TextInputFormatters.fWholeNumber,
                        decoration: InputDecoration(
                          hintText: _l10n.searchWorldsScreen_label_authorId,
                          labelText: _l10n.searchWorldsScreen_label_authorId,
                          suffixIcon: IconButton(
                            icon: Icon(Icons.clear),
                            onPressed: authorIdClearPressedListener,
                          ),
                        ),
                      ),
                    ),
                    FlatButton(
                      onPressed: searchPressedListener,
                      child: Text(_l10n.searchWorldsScreen_button_search),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void barTapListener({bool isUp = false}) {
    FocusScope.of(context).unfocus();
    setState(() {
      isOpen = (isUp) ? false : !isOpen;
    });
  }

  void clearPressedListener() {
    nameClearPressedListener();
    authorNameClearPressedListener();
    authorIdClearPressedListener();
    searchPressedListener(isUp: true);
  }

  void nameClearPressedListener() {
    _nameController.clear();
  }

  void authorNameClearPressedListener() {
    _authorNameController.clear();
  }

  void authorIdClearPressedListener() {
    _authorIdController.clear();
  }

  void searchPressedListener({bool isUp = false}) {
    var authIdString = _authorIdController.text;
    widget.cubit.search(
      SearchParameter(
        name: _nameController.text,
        authorName: _authorNameController.text,
        authorId: (authIdString != null && authIdString.isNotEmpty)
            ? _authorIdController.text
            : null,
      ),
    );
    barTapListener(isUp: isUp);
  }
}

class _SearchLabel extends StatelessWidget {
  final String title;

  const _SearchLabel({Key key, @required this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var textStyle = Theme.of(context).textTheme.bodyText1;
    return Container(
      margin: Indents.kDefaultLeftIndention,
      padding: Indents.kDefaultHorizontalIndention,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.horizontal(
          left: const Radius.circular(10.0),
          right: const Radius.circular(10.0),
        ),
        border: Border.all(color: textStyle.color),
      ),
      child: Text(
        title,
        style: textStyle,
      ),
    );
  }
}
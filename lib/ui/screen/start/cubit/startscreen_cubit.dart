import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/domain/user_center.dart';
import 'package:wonderful_world/domain/world_center.dart';
import 'package:wonderful_world/ui/cubit/theme_cubit.dart';
import 'package:wonderful_world/ui/router/router.gr.dart';

part 'startscreen_state.dart';

class StartscreenCubit extends Cubit<StartscreenState> {
  final ThemeCubit _themeCubit;
  final UserCenter _userCenter;
  final WorldCenter _worldCenter;
  StartscreenCubit(this._themeCubit, this._userCenter, this._worldCenter)
      : super(StartscreenAwait());

  void chek() {
    _setAwait();
    _themeCubit.update().then((_) {
      _userCenter.isAuth().then((_) {
        ExtendedNavigator.root
            .pushAndRemoveUntil(Routes.homeScreen, (route) => false);
      }).catchError((_) {
        _worldCenter.getLastWorld().then((world) {
        emit(StartscreenFail(world));
        });
      });
    });
  }

  void closePressedListener() {
    SystemNavigator.pop();
  }

  void lastWorldOpenListener() {
    if (state.isLastWorldExists) {
    ExtendedNavigator.root.push(
      Routes.reflectionScreen,
      arguments: ReflectionScreenArguments(
        id: state.lastWorld.id,
        ownerId: state.lastWorld.authorId,
        type: state.lastWorld.type,
        worldWidth: state.lastWorld.width,
        worldHeight: state.lastWorld.height,
        isLast: true,
      ),
    );
    }
  }

  void _setAwait() {
    var isAwaiting = this.state is StartscreenAwait;
    if (!isAwaiting) {
      emit(StartscreenAwait());
    }
  }
}

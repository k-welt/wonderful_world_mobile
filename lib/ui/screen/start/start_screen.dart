import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/screen/start/cubit/startscreen_cubit.dart';
import 'package:wonderful_world/ui/widget/awaiting.dart';

class StartScreen extends StatefulWidget {
  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> with ScopeStateMixin {
  StartscreenCubit _cubit;
  S _l10n;

  @override
  void initState() {
    super.initState();
    _cubit = currentScope.get();
    _cubit.chek();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<StartscreenCubit, StartscreenState>(
        cubit: _cubit,
        builder: (context, state) {
          if (state is StartscreenAwait) {
            return Awaiting();
          } else {
            // AuthStateFail
            return Align(
              alignment: const Alignment(0.0, 0.50),
              child: Card(
                child: ListTile(
                  title: Container(
                    margin: Indents.kDefaultBottomIndention,
                    child: Text(
                      _l10n.startScreen_text_failMessage,
                      style: TextStyle(color: Colors.red),
                    ),
                  ),
                  subtitle: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      FlatButton(
                          onPressed: _cubit.chek,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(_l10n.startScreen_button_tryAgain),
                              Spacer(),
                              Icon(Icons.replay),
                            ],
                          )),
                      Divider(),
                      FlatButton(
                        onPressed: state.isLastWorldExists
                            ? _cubit.lastWorldOpenListener
                            : null,
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(_l10n.startScreen_button_lastWorldOpen),
                            Spacer(),
                            Icon(Icons.keyboard_arrow_right),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      ),
      bottomSheet: Container(
        width: double.infinity,
        child: FlatButton(
          child: Text(_l10n.startScreen_button_closeApp),
          onPressed: _cubit.closePressedListener,
        ),
      ),
    );
  }
}

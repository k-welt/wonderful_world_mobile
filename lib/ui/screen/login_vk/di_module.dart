import 'package:koin/koin.dart';
import 'package:koin_bloc/koin_bloc.dart';
import 'cubit/loginvkscreen_cubit.dart';
import 'loginvk_screen.dart';

var loginVkModule = Module()
  ..scopeOneCubit<LoginvkscreenCubit, LoginVkScreen>(
    (scope) => LoginvkscreenCubit(scope.get()),
  );
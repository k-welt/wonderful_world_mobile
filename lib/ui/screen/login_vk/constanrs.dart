const kAccessRedirectUrl = 'https://cwelt.net/api/auth/vk';
const kGrantedRedirect = 'https://cwelt.net/api/auth/vk?code=';
const kErrorRedirect = 'https://cwelt.net/api/auth/vk?error=access_denied';
const kErrorEmailRedirect = 'email_denied';
const kOauthVkUrl = 'https://oauth.vk.com/authorize' +
    '?client_id=7662779' +
    '&display=mobile' +
    '&redirect_uri=' +
    kAccessRedirectUrl +
    '&scope=email' +
    '&response_type=code&v=5.126';

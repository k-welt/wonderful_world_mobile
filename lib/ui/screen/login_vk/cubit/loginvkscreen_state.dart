part of 'loginvkscreen_cubit.dart';

@immutable
abstract class LoginvkscreenState {}

class LoginvkscreenInitial extends LoginvkscreenState {}

class LoginvkscreenAwait extends LoginvkscreenState {}

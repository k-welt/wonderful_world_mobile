import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:wonderful_world/domain/user_center.dart';

import '../constanrs.dart';

part 'loginvkscreen_state.dart';

class LoginvkscreenCubit extends Cubit<LoginvkscreenState> {
  UserCenter _userCenter;
  StreamSubscription<bool> _authStateChangeSubscription;
  LoginvkscreenCubit(this._userCenter) : super(LoginvkscreenInitial()) {
    _authStateChangeSubscription =
        _userCenter.authStateChangeObservable.listen(_authStateChangedListener);
  }

  void _authStateChangedListener(bool isAuth) {
    if (isAuth) {
      ExtendedNavigator.root.pop(true);
    } else {
      _setDefault();
    }
  }

  NavigationDecision redirectListener(NavigationRequest navigation) {
    var url = navigation.url;
    if (url.startsWith(kErrorRedirect) || url.endsWith(kErrorEmailRedirect)) {
      ExtendedNavigator.root.pop(false);
    } else if (url.startsWith(kGrantedRedirect)) {
      _setAwait();
      _userCenter.loginVk(url.substring(kGrantedRedirect.length, url.length));
    } else {
      return NavigationDecision.navigate;
    }
    return NavigationDecision.prevent;
  }

  void _setDefault() {
    emit(LoginvkscreenInitial());
  }

  void _setAwait() {
    emit(LoginvkscreenAwait());
  }

  @override
  Future<void> close() async {
    _authStateChangeSubscription.cancel();
    await super.close();
  }
}

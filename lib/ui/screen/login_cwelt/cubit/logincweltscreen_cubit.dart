import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:wonderful_world/domain/user_center.dart';

part 'logincweltscreen_state.dart';

class LogincweltscreenCubit extends Cubit<LogincweltscreenState> {
  final UserCenter _userCenter;
  StreamSubscription<bool> _authStateChangeSubscription;
  LogincweltscreenCubit(this._userCenter) : super(LogincweltscreenInitial()) {
    _authStateChangeSubscription =
        _userCenter.authStateChangeObservable.listen(_authStateChangedListener);
  }

  void _authStateChangedListener(bool isAuth) {
    if (isAuth) {
      ExtendedNavigator.root.pop(true);
    } else {
      setDefault();
    }
  }

  void usernameAndEmailCheck(String username, String email) {
    _userCenter.existsUsername(username).then((value) {
      if (value) {
        emit(LogincweltscreenUsernameExists());
      } else {
        _userCenter.existsEmail(email).then((value) {
          if (value) {
            emit(LogincweltscreenEmailExists());
          } else {
            setCorrect();
          }
        }).catchError((_) {
          setCorrect();
        });
      }
    });
  }

  void register(String username, String email, String password) {
    _setAwait();
    _userCenter.register(username, email, password);
  }

  void login(String email, String password) {
    _setAwait();
    _userCenter.login(email, password);
  }

  void setDefault() {
    emit(LogincweltscreenInitial());
  }

  void setCorrect() {
    emit(LogincweltscreenCorrect());
  }

  void _setAwait() {
    emit(LogincweltscreenAwait());
  }

  @override
  Future<void> close() async {
    _authStateChangeSubscription.cancel();
    await super.close();
  }
}

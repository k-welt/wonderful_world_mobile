import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:koin_flutter/koin_flutter.dart';
import 'package:wonderful_world/generated/l10n.dart';
import 'package:wonderful_world/ui/constants/indents.dart';
import 'package:wonderful_world/ui/constants/regexps.dart';
import 'package:wonderful_world/ui/constants/textinput_formatters.dart';
import 'package:wonderful_world/ui/widget/awaiting_button.dart';

import '../cubit/logincweltscreen_cubit.dart';

class SignupComponent extends StatefulWidget {
  @override
  _SignupComponentState createState() => _SignupComponentState();
}

class _SignupComponentState extends State<SignupComponent> with ScopeStateMixin {
  S _l10n;
  LogincweltscreenCubit _loginscreenCubit;

  bool _isUsernameValidate = false;
  bool _isEmailValidate = false;
  bool _isPasswordValidate = false;
  bool _isRepeatPasswordValidate = false;

  String _username;
  String _email;
  String _password;

  @override
  void initState() {
    super.initState();
    _loginscreenCubit = currentScope.get();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _l10n = S.of(context);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
        value: _loginscreenCubit,
        child: Column(
          children: [
            Padding(
              // username
              padding: Indents.kCardIndention,
              child: TextFormField(
                autovalidateMode: AutovalidateMode.always,
                validator: _usernameValidator,
                inputFormatters: TextInputFormatters.fBaseName,
                decoration: InputDecoration(
                  hintText: _l10n.loginScreen_input_username,
                  labelText: _l10n.loginScreen_input_username,
                ),
              ),
            ),
            BlocBuilder<LogincweltscreenCubit, LogincweltscreenState>(builder: (_, state) {
              return (state is LogincweltscreenUsernameExists)
                  ? Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                        _l10n.loginScreen_input_username_exists,
                        style: TextStyle(color: Colors.red),
                      ),
                    )
                  : Container();
            }),
            Padding(
              // email
              padding: Indents.kCardIndention,
              child: TextFormField(
                autovalidateMode: AutovalidateMode.always,
                validator: _emailValidator,
                inputFormatters: TextInputFormatters.fEmail,
                decoration: InputDecoration(
                  hintText: _l10n.loginScreen_input_email,
                  labelText: _l10n.loginScreen_input_email,
                ),
              ),
            ),
            BlocBuilder<LogincweltscreenCubit, LogincweltscreenState>(builder: (_, state) {
              return (state is LogincweltscreenEmailExists)
                  ? Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Text(
                        _l10n.loginScreen_input_email_exists,
                        style: TextStyle(color: Colors.red),
                      ),
                    )
                  : Container();
            }),
            Form(
                child: Column(
              children: [
                Padding(
                  //  password
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    obscureText: true,
                    autovalidateMode: AutovalidateMode.always,
                    validator: _passwordValidator,
                    inputFormatters: TextInputFormatters.fBaseName,
                    decoration: InputDecoration(
                      hintText: _l10n.loginScreen_input_password,
                      labelText: _l10n.loginScreen_input_password,
                    ),
                  ),
                ),
                Padding(
                  //  repeat password
                  padding: Indents.kCardIndention,
                  child: TextFormField(
                    obscureText: true,
                    autovalidateMode: AutovalidateMode.always,
                    validator: _repeatPasswordValidator,
                    inputFormatters: TextInputFormatters.fBaseName,
                    decoration: InputDecoration(
                      hintText: _l10n.loginScreen_input_repeatPassword,
                      labelText: _l10n.loginScreen_input_repeatPassword,
                    ),
                  ),
                )
              ],
            )),
            Padding(
              padding: Indents.kCardIndention,
              child: Row(
                children: [
                  Expanded(
                    child: AwaitingButton<LogincweltscreenCubit, LogincweltscreenState>(
                      title: _l10n.loginScreen_button_signUp,
                      onPressed: _onSignUpPressedListener,
                      isEnabledDelegat: (state) => state is LogincweltscreenCorrect,
                      isAwaitDelegat: (state) => state is LogincweltscreenAwait,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
    );
  }

  String _usernameValidator(String value) {
    String result;
    bool stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.loginScreen_input_username_validate_fail;
    }
    _username = value;
    if (_isUsernameValidate != stepValidate || stepValidate) {
      _isUsernameValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  String _emailValidator(String value) {
    String result;
    bool stepValidate =
        value != null && value.isNotEmpty && RegExps.fEmail.hasMatch(value);
    if (!stepValidate) {
      result = _l10n.loginScreen_input_email_validate_fail;
    }
    _email = value;
    if (_isEmailValidate != stepValidate || stepValidate) {
      _isEmailValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  String _passwordValidator(String value) {
    String result;
    bool stepValidate = value != null && value.isNotEmpty;
    if (!stepValidate) {
      result = _l10n.loginScreen_input_password_validate_fail;
    }
    _password = value;
    _isPasswordValidate = stepValidate;
    return result;
  }

  String _repeatPasswordValidator(String value) {
    String result;
    bool stepValidate = value != null &&
        value.isNotEmpty &&
        _password != null &&
        value.compareTo(_password) == 0;
    if (!stepValidate) {
      result = _l10n.loginScreen_input_repeatPassword_validate_fail;
    }
    if (_isRepeatPasswordValidate != stepValidate || stepValidate) {
      _isRepeatPasswordValidate = stepValidate;
      _changedListener();
    }
    return result;
  }

  bool get isAllValidate =>
      _isUsernameValidate &&
      _isEmailValidate &&
      _isPasswordValidate &&
      _isRepeatPasswordValidate;

  Future<void> _changedListener() async {
    if (isAllValidate) {
      _loginscreenCubit.usernameAndEmailCheck(_username, _email);
    }

    if (_loginscreenCubit.state is LogincweltscreenCorrect) {
      _loginscreenCubit.setDefault();
    }
  }

  void _onSignUpPressedListener() {
    if (isAllValidate) {
      _loginscreenCubit.register(_username, _email, _password);
    }
  }
}

import 'package:flutter/services.dart';

class TextInputFormatters {
  static final fBaseName = <TextInputFormatter>[
    LengthLimitingTextInputFormatter(20),
    FilteringTextInputFormatter(RegExp(r"[a-zA-Z0-9 ]"), allow: true)
  ];
  static final fSearchBaseName = <TextInputFormatter>[
    LengthLimitingTextInputFormatter(20),
    FilteringTextInputFormatter(RegExp(r"[a-zA-Z0-9 _]"), allow: true)
  ];
  static final fEmail = <TextInputFormatter>[
    LengthLimitingTextInputFormatter(50)
  ];
  static final fText = <TextInputFormatter>[
    LengthLimitingTextInputFormatter(1000)
  ];
  static final fWholeNumber = <TextInputFormatter>[
    LengthLimitingTextInputFormatter(10),
    FilteringTextInputFormatter(RegExp(r"[0-9]"), allow: true)
  ];
}
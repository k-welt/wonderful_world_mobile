import 'package:koin_flutter/koin_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/model/login/login_body.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/user/user.dart';

abstract class UserRepository implements Disposable {
  BehaviorSubject<TokenUpdatingState> get tokenUpdateSubject;
  Future<bool> tokenUpdate();

  Future<String> token();

  Future<bool> check();

  Future<bool> register(RegisterBody registerBody);

  Future<bool> login(LoginBody loginBody);

  Future<bool> loginVk(String code);

  Future<bool> logout();

  Future<User> user(String id);

  Future<User> current();

  Future<bool> existsUsername(String username);

  Future<bool> existsEmail(String email);
}

enum TokenUpdatingState {
  processing,
  success,
  failed,
}

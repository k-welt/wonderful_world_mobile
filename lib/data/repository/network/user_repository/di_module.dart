import 'package:koin/koin.dart';
import 'package:koin_flutter/koin_disposable.dart';

import 'user_repository.dart';
import 'user_repository_impl.dart';

var userRepositoryModule = Module()
..disposable<UserRepository>(
    (scope) => UserRepositoryImpl(scope.get(), scope.get()),
  );

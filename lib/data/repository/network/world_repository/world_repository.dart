import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';

abstract class WorldRepository {
  Future<World> world(String id);

  Future<List<String>> worlds();

  Future<List<String>> selectedWorlds();

  Future<List<String>> createdWorlds();

  Future<List<String>> search({
    String name,
    String authorId,
    String authorName,
  });

  Future<bool> add(WorldAddReqBody world);

  Future<bool> clone(String id);

  Future<bool> remove(String id);

  Future<bool> selectedWorldAdd(String id);

  Future<bool> selectedWorldRemove(String id);
}
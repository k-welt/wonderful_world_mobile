import 'package:koin/koin.dart';
import 'world_repository.dart';
import 'world_repository_impl.dart';

var worldRepositoryModule = Module()
  ..single<WorldRepository>(
    (scope) => WorldRepositoryImpl(scope.get()),
  );

import 'package:wonderful_world/data/data_source/local/db/db_store.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/repository/local/db_repository/db_repository.dart';

class DbRepositoryImpl implements DbRepository {
  static const int _kLastWorldKey = 0;
  final DbStore _store;

  DbRepositoryImpl(this._store);

  @override
  Future<World> loadLastWorld() async {
    return await _store.loadWorld(_kLastWorldKey);
  }

  @override
  Future<void> saveLastWorld(World world) async {
    await _store.saveWorld(_kLastWorldKey, world);
  }

  @override
  Future<WorldData> loadLastWorldData() async {
    var bufferWorld = await loadLastWorld();
    return WorldData(
      await _store.loadWorldDataComponents(_kLastWorldKey),
      type: bufferWorld.type,
      size: NaturalSize(
        bufferWorld.width,
        bufferWorld.height,
      ),
    );
  }

  @override
  Future<void> saveLastWorldData(WorldComponents data) async {
    await _store.saveWorldDataComponents(_kLastWorldKey, data);
  }
}

import 'package:koin/koin.dart';
import 'package:wonderful_world/data/repository/local/db_repository/db_repository.dart';
import 'package:wonderful_world/data/repository/local/db_repository/db_repository_impl.dart';

var dbRpositoryModule = Module()
  ..single<DbRepository>(
    (scope) => DbRepositoryImpl(scope.get()),
  );

import 'package:koin/koin.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/settings_repository.dart';
import 'package:wonderful_world/data/repository/local/settings_repository/settings_repository_impl.dart';

var settingsRepositoryModule = Module()
  ..single<SettingsRepository>(
    (scope) => SettingsRepositoryImpl(scope.get()),
  );

abstract class SettingsRepository {
  Future<bool> getDarkModeState();
  Future<void> setDarkModeState(bool state);
  Future<int> getReflectionBackgroundColor();
  Future<void> setReflectionBackgroundColor(int color);
  Future<int> getReflectionGreedColor();
  Future<void> setReflectionGreedColor(int color);
  Future<int> getReflectionCell0Color();
  Future<void> setReflectionCell0Color(int color);
}

import 'package:wonderful_world/data/repository/local/di_module.dart';

import 'network/di_module.dart';

var repositoryModules = [
  ...localRepositoryModules,
  ...networkRepositoryModules,
];

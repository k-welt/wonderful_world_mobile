import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
  final String id;
  final String username;
  final String email;
  final DateTime createTimestamp;
  final int createdWorldCount;
  final int selectedWorldCount;

  User({
    this.id,
    this.username,
    this.email,
    this.createTimestamp,
    this.createdWorldCount,
    this.selectedWorldCount,
  });

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);
}

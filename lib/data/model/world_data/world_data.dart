import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'world_data.g.dart';

@JsonSerializable()
class WorldData {
  final WorldComponents data;
  final int type;
  final NaturalSize size;

  WorldData(this.data, {this.type, this.size});

  factory WorldData.fromJson(Map<String, dynamic> json) =>
      _$WorldDataFromJson(json);
  Map<String, dynamic> toJson() => _$WorldDataToJson(this);
}

@JsonSerializable()
class WorldComponents {
  final List<CellT0> cells0;

  WorldComponents(this.cells0);

  factory WorldComponents.fromJson(Map<String, dynamic> json) => _$WorldComponentsFromJson(json);
  Map<String, dynamic> toJson() => _$WorldComponentsToJson(this);
}

@HiveType(typeId: 1)
@JsonSerializable()
class CellT0 {
  @HiveField(0)
  final int x;
  @HiveField(1)
  final int y;

  CellT0(this.x, this.y);

  factory CellT0.fromJson(Map<String, dynamic> json) =>
      _$CellT0FromJson(json);
  Map<String, dynamic> toJson() => _$CellT0ToJson(this);
}

@JsonSerializable()
class NaturalSize {
  final int width;
  final int height;

  NaturalSize(this.width, this.height);

  factory NaturalSize.fromJson(Map<String, dynamic> json) =>
      _$NaturalSizeFromJson(json);
  Map<String, dynamic> toJson() => _$NaturalSizeToJson(this);
}

import 'package:json_annotation/json_annotation.dart';

part 'world_add_req_body.g.dart';

@JsonSerializable()
class WorldAddReqBody {
  final String name;
  final String description;
  final int width;
  final int height;
  final int type;

  WorldAddReqBody(
    this.name,
    this.description,
    this.width,
    this.height,
    this.type,
  );

  factory WorldAddReqBody.fromJson(Map<String, dynamic> json) =>
      _$WorldAddReqBodyFromJson(json);
  Map<String, dynamic> toJson() => _$WorldAddReqBodyToJson(this);
}

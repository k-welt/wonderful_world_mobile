import 'package:koin/koin.dart';

import 'db_store.dart';
import 'db_store_impl.dart';

Module getDbStoreModule(String path) {
  return Module()..single<DbStore>((scope) => DbStoreImpl(path: path));
}

var dbStoreModule = Module()..single<DbStore>((scope) => DbStoreImpl());

import 'secure_references/secure_preferences_store.dart';

class TokenStore {
  static const kTokenKey = 'sec_token';
  static const kRefreshToken = 'sec_refresh_token';

  final SecurePreferencesStore _store;
  const TokenStore(this._store);

  Future<String> get() async {
    return await _store.getString(kTokenKey);
  }

  Future<String> getRefresh() async {
    return await _store.getString(kRefreshToken);
  }

  Future<bool> set(String token) async {
    return await _store.setString(kTokenKey, token);
  }

  Future<bool> setRefresh(String token) async {
    return await _store.setString(kRefreshToken, token);
  }

  Future<void> reload() async {
    await _store.reload();
  }
}

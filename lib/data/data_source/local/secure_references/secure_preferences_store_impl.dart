import 'package:encrypted_shared_preferences/encrypted_shared_preferences.dart';
import 'package:wonderful_world/data/data_source/local/secure_references/secure_preferences_store.dart';

class SecurePreferencesStoreImpl implements SecurePreferencesStore {
  EncryptedSharedPreferences _prefs;

  Future<void> _initPrefs() async {
    _prefs ??= EncryptedSharedPreferences();
  }

  @override
  Future<bool> clear() async {
    await _initPrefs();
    return await _prefs.clear();
  }

  @override
  Future<String> getString(String key) async {
    await _initPrefs();
    return await _prefs.getString(key);
  }

  @override
  Future<void> reload() async {
    await _initPrefs();
    return await _prefs.reload();
  }

  @override
  Future<bool> setString(String key, String value) async {
    await _initPrefs();
    return await _prefs.setString(key, value);
  }
}

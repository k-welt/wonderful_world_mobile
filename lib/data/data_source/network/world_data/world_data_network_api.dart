import 'dart:io';

import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';

import '../constants.dart';

part 'world_data_network_api.g.dart';

@RestApi(baseUrl: kBaseUrl)
abstract class WorldDataNetworkApi {
  factory WorldDataNetworkApi(Dio dio, {String baseUrl}) = _WorldDataNetworkApi;

  @GET(kPathApiDataLoad)
  Future<WorldData> getWorldData(
    @Path('id') String id,
  );

  @POST(kPathApiDataSave)
  Future<void> saveWorldData(
    @Header(HttpHeaders.authorizationHeader) String authorization,
    @Path('id') String id,
    @Body() WorldComponents worldData,
  );
}

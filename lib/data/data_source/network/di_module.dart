import 'package:dio/dio.dart';
import 'package:koin/koin.dart';
import 'network_api.dart';
import 'network_api_helper.dart';
import 'network_api_helper_impl.dart';
import 'token_provider.dart';

var networkModule = Module()
  ..single((scope) => TokenProvider(scope.get()))
  ..single((scope) => Dio())
  ..single((scope) => NetworkApi(scope.get()))
  ..single<NetworkApiHelper>(
    (scope) => NetworkApiHelperImpl(scope.get(), scope.get()),
  );

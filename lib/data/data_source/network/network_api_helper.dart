import 'package:wonderful_world/data/model/login/login_body.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/token/token.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_add_req_body/world_add_req_body.dart';

abstract class NetworkApiHelper {
  Future<bool> check();

  Future<Token> loginVk(String code);

  Future<Token> login(LoginBody loginBody);

  Future<Token> register(RegisterBody registerBody);

  Future<Token> tokenUpdate();

  Future<bool> logout();

  Future<User> user(String id);

  Future<User> current();

  Future<bool> existsUsername(String username);

  Future<bool> existsEmail(String email);

  Future<World> world(String id);

  Future<List<String>> worlds();

  Future<List<String>> selectedWorlds();

  Future<List<String>> createdWorlds();

  Future<List<String>> search({
    String name,
    String authorId,
    String authorName,
  });

  Future<bool> add(WorldAddReqBody world);

  Future<bool> clone(String id);

  Future<bool> remove(String id);

  Future<bool> selectedWorldAdd(String id);

  Future<bool> selectedWorldRemove(String id);
}

// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static m0(count) => "Created worlds: ${count}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "button_cancel" : MessageLookupByLibrary.simpleMessage("Cancel"),
    "button_ok" : MessageLookupByLibrary.simpleMessage("Ok"),
    "cell_type_0" : MessageLookupByLibrary.simpleMessage("Game of Life (B2/S23 - M)"),
    "cell_type_1" : MessageLookupByLibrary.simpleMessage("Game of Life (B3/S012345678 - M)"),
    "cell_type_2" : MessageLookupByLibrary.simpleMessage("Game of Life (B1/S012345678 - M)"),
    "cell_type_3" : MessageLookupByLibrary.simpleMessage("Game of Life (B1/S012345678 - N)"),
    "cell_type_4" : MessageLookupByLibrary.simpleMessage("Day & Night (B3678/S34678 - M)"),
    "cell_type_99" : MessageLookupByLibrary.simpleMessage("New Type"),
    "contextMenu_item_clone" : MessageLookupByLibrary.simpleMessage("Clone"),
    "contextMenu_item_remove" : MessageLookupByLibrary.simpleMessage("Remove"),
    "contextMenu_item_selected_add" : MessageLookupByLibrary.simpleMessage("Add to favourites"),
    "contextMenu_item_selected_remove" : MessageLookupByLibrary.simpleMessage("Remove from favorites"),
    "createWorldScreen_button_create" : MessageLookupByLibrary.simpleMessage("Create"),
    "createWorldScreen_input_description" : MessageLookupByLibrary.simpleMessage("Description"),
    "createWorldScreen_input_description_validate_fail" : MessageLookupByLibrary.simpleMessage("Please enter an description"),
    "createWorldScreen_input_height" : MessageLookupByLibrary.simpleMessage("Height"),
    "createWorldScreen_input_height_validate_fail_0" : MessageLookupByLibrary.simpleMessage("Please enter an height"),
    "createWorldScreen_input_height_validate_fail_1" : MessageLookupByLibrary.simpleMessage(" > 0"),
    "createWorldScreen_input_title" : MessageLookupByLibrary.simpleMessage("Title"),
    "createWorldScreen_input_title_validate_fail" : MessageLookupByLibrary.simpleMessage("Please enter an title"),
    "createWorldScreen_input_type" : MessageLookupByLibrary.simpleMessage("Type"),
    "createWorldScreen_input_width" : MessageLookupByLibrary.simpleMessage("Width"),
    "createWorldScreen_input_width_validate_fail_0" : MessageLookupByLibrary.simpleMessage("Please enter an width"),
    "createWorldScreen_input_width_validate_fail_1" : MessageLookupByLibrary.simpleMessage(" > 0"),
    "createWorldScreen_title" : MessageLookupByLibrary.simpleMessage("Creating a world"),
    "currentUserScreen_button_logout" : MessageLookupByLibrary.simpleMessage("Logout"),
    "currentUserScreen_title" : MessageLookupByLibrary.simpleMessage("Current user"),
    "homeScreen_drawer_darkTheme" : MessageLookupByLibrary.simpleMessage("Dark theme"),
    "homeScreen_drawer_userCard_button_auth" : MessageLookupByLibrary.simpleMessage("Go to profile"),
    "homeScreen_drawer_userCard_button_unauth" : MessageLookupByLibrary.simpleMessage("Sign in"),
    "homeScreen_drawer_userCard_title_default" : MessageLookupByLibrary.simpleMessage("User is missing"),
    "homeScreen_title_available" : MessageLookupByLibrary.simpleMessage("Available worlds"),
    "homeScreen_title_created" : MessageLookupByLibrary.simpleMessage("Created worlds"),
    "homeScreen_title_selected" : MessageLookupByLibrary.simpleMessage("Selected worlds"),
    "loginScreen_button_logIn" : MessageLookupByLibrary.simpleMessage("LogIn"),
    "loginScreen_button_signUp" : MessageLookupByLibrary.simpleMessage("Register"),
    "loginScreen_input_email" : MessageLookupByLibrary.simpleMessage("E-Mail"),
    "loginScreen_input_email_exists" : MessageLookupByLibrary.simpleMessage("Email exists"),
    "loginScreen_input_email_validate_fail" : MessageLookupByLibrary.simpleMessage("Please enter an Email address"),
    "loginScreen_input_password" : MessageLookupByLibrary.simpleMessage("Password"),
    "loginScreen_input_password_validate_fail" : MessageLookupByLibrary.simpleMessage("Please enter a password"),
    "loginScreen_input_repeatPassword" : MessageLookupByLibrary.simpleMessage("Verify password"),
    "loginScreen_input_repeatPassword_validate_fail" : MessageLookupByLibrary.simpleMessage("Please repeat your password"),
    "loginScreen_input_username" : MessageLookupByLibrary.simpleMessage("Username"),
    "loginScreen_input_username_exists" : MessageLookupByLibrary.simpleMessage("Username exists"),
    "loginScreen_input_username_validate_fail" : MessageLookupByLibrary.simpleMessage("Please enter an username"),
    "loginScreen_link_logIn" : MessageLookupByLibrary.simpleMessage("Already Registered? Log In Here"),
    "loginScreen_link_signUp" : MessageLookupByLibrary.simpleMessage("Not Registered? Sign Up Here"),
    "loginScreen_title_logIn" : MessageLookupByLibrary.simpleMessage("Log In"),
    "loginScreen_title_signUp" : MessageLookupByLibrary.simpleMessage("Sign Up"),
    "notification_content_add" : MessageLookupByLibrary.simpleMessage("Added"),
    "notification_content_clone" : MessageLookupByLibrary.simpleMessage("Copied"),
    "notification_content_create" : MessageLookupByLibrary.simpleMessage("Created"),
    "notification_content_delete" : MessageLookupByLibrary.simpleMessage("Removed!"),
    "reflectionScreen_button_options" : MessageLookupByLibrary.simpleMessage("Options"),
    "reflectionScreen_button_play" : MessageLookupByLibrary.simpleMessage("Play"),
    "reflectionScreen_button_stop" : MessageLookupByLibrary.simpleMessage("Stop"),
    "reflectionScreen_card_loadingFail" : MessageLookupByLibrary.simpleMessage("Download failed"),
    "reflectionScreen_panel_browsing_item_background" : MessageLookupByLibrary.simpleMessage("Background"),
    "reflectionScreen_panel_browsing_item_color_0_gof" : MessageLookupByLibrary.simpleMessage("Cell"),
    "reflectionScreen_panel_browsing_item_grid" : MessageLookupByLibrary.simpleMessage("Grid"),
    "reflectionScreen_panel_browsing_item_reset" : MessageLookupByLibrary.simpleMessage("Reset"),
    "reflectionScreen_panel_editing_item_pain_0_gof" : MessageLookupByLibrary.simpleMessage("Cell"),
    "reflectionScreen_panel_editing_item_remove" : MessageLookupByLibrary.simpleMessage("Remove"),
    "reflectionScreen_title_browsing" : MessageLookupByLibrary.simpleMessage("Browsing"),
    "reflectionScreen_title_editing" : MessageLookupByLibrary.simpleMessage("Editing"),
    "reflectionScreen_title_loading" : MessageLookupByLibrary.simpleMessage("Loading"),
    "searchWorldsScreen_button_search" : MessageLookupByLibrary.simpleMessage("Search"),
    "searchWorldsScreen_label_authorId" : MessageLookupByLibrary.simpleMessage("Author Id"),
    "searchWorldsScreen_label_authorName" : MessageLookupByLibrary.simpleMessage("Author"),
    "searchWorldsScreen_label_name" : MessageLookupByLibrary.simpleMessage("Title"),
    "searchWorldsScreen_title" : MessageLookupByLibrary.simpleMessage("Search"),
    "startScreen_button_closeApp" : MessageLookupByLibrary.simpleMessage("Close application"),
    "startScreen_button_lastWorldOpen" : MessageLookupByLibrary.simpleMessage("Last open world"),
    "startScreen_button_tryAgain" : MessageLookupByLibrary.simpleMessage("Try again"),
    "startScreen_text_failMessage" : MessageLookupByLibrary.simpleMessage("Connection failure"),
    "userWidget_card_email" : MessageLookupByLibrary.simpleMessage("E-Mail:"),
    "userWidget_card_id" : MessageLookupByLibrary.simpleMessage("Id:"),
    "userWidget_card_username" : MessageLookupByLibrary.simpleMessage("Username:"),
    "userWidget_card_worldsCreated" : m0,
    "userWidget_card_worldsCreated_button" : MessageLookupByLibrary.simpleMessage("Показать"),
    "worldScreen_button_open" : MessageLookupByLibrary.simpleMessage("Open"),
    "worldScreen_card_description" : MessageLookupByLibrary.simpleMessage("Description:"),
    "worldScreen_card_id" : MessageLookupByLibrary.simpleMessage("Id:"),
    "worldScreen_card_owner" : MessageLookupByLibrary.simpleMessage("Owner:"),
    "worldScreen_card_size" : MessageLookupByLibrary.simpleMessage("Size:"),
    "worldScreen_card_title" : MessageLookupByLibrary.simpleMessage("Title:"),
    "worldScreen_card_type" : MessageLookupByLibrary.simpleMessage("Type:")
  };
}

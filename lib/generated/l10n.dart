// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Ok`
  String get button_ok {
    return Intl.message(
      'Ok',
      name: 'button_ok',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get button_cancel {
    return Intl.message(
      'Cancel',
      name: 'button_cancel',
      desc: '',
      args: [],
    );
  }

  /// `Game of Life (B2/S23 - M)`
  String get cell_type_0 {
    return Intl.message(
      'Game of Life (B2/S23 - M)',
      name: 'cell_type_0',
      desc: '',
      args: [],
    );
  }

  /// `Game of Life (B3/S012345678 - M)`
  String get cell_type_1 {
    return Intl.message(
      'Game of Life (B3/S012345678 - M)',
      name: 'cell_type_1',
      desc: '',
      args: [],
    );
  }

  /// `Game of Life (B1/S012345678 - M)`
  String get cell_type_2 {
    return Intl.message(
      'Game of Life (B1/S012345678 - M)',
      name: 'cell_type_2',
      desc: '',
      args: [],
    );
  }

  /// `Game of Life (B1/S012345678 - N)`
  String get cell_type_3 {
    return Intl.message(
      'Game of Life (B1/S012345678 - N)',
      name: 'cell_type_3',
      desc: '',
      args: [],
    );
  }

  /// `Day & Night (B3678/S34678 - M)`
  String get cell_type_4 {
    return Intl.message(
      'Day & Night (B3678/S34678 - M)',
      name: 'cell_type_4',
      desc: '',
      args: [],
    );
  }

  /// `New Type`
  String get cell_type_99 {
    return Intl.message(
      'New Type',
      name: 'cell_type_99',
      desc: '',
      args: [],
    );
  }

  /// `Close application`
  String get startScreen_button_closeApp {
    return Intl.message(
      'Close application',
      name: 'startScreen_button_closeApp',
      desc: '',
      args: [],
    );
  }

  /// `Try again`
  String get startScreen_button_tryAgain {
    return Intl.message(
      'Try again',
      name: 'startScreen_button_tryAgain',
      desc: '',
      args: [],
    );
  }

  /// `Last open world`
  String get startScreen_button_lastWorldOpen {
    return Intl.message(
      'Last open world',
      name: 'startScreen_button_lastWorldOpen',
      desc: '',
      args: [],
    );
  }

  /// `Connection failure`
  String get startScreen_text_failMessage {
    return Intl.message(
      'Connection failure',
      name: 'startScreen_text_failMessage',
      desc: '',
      args: [],
    );
  }

  /// `Add to favourites`
  String get contextMenu_item_selected_add {
    return Intl.message(
      'Add to favourites',
      name: 'contextMenu_item_selected_add',
      desc: '',
      args: [],
    );
  }

  /// `Remove from favorites`
  String get contextMenu_item_selected_remove {
    return Intl.message(
      'Remove from favorites',
      name: 'contextMenu_item_selected_remove',
      desc: '',
      args: [],
    );
  }

  /// `Remove`
  String get contextMenu_item_remove {
    return Intl.message(
      'Remove',
      name: 'contextMenu_item_remove',
      desc: '',
      args: [],
    );
  }

  /// `Clone`
  String get contextMenu_item_clone {
    return Intl.message(
      'Clone',
      name: 'contextMenu_item_clone',
      desc: '',
      args: [],
    );
  }

  /// `Created`
  String get notification_content_create {
    return Intl.message(
      'Created',
      name: 'notification_content_create',
      desc: '',
      args: [],
    );
  }

  /// `Added`
  String get notification_content_add {
    return Intl.message(
      'Added',
      name: 'notification_content_add',
      desc: '',
      args: [],
    );
  }

  /// `Copied`
  String get notification_content_clone {
    return Intl.message(
      'Copied',
      name: 'notification_content_clone',
      desc: '',
      args: [],
    );
  }

  /// `Removed!`
  String get notification_content_delete {
    return Intl.message(
      'Removed!',
      name: 'notification_content_delete',
      desc: '',
      args: [],
    );
  }

  /// `Available worlds`
  String get homeScreen_title_available {
    return Intl.message(
      'Available worlds',
      name: 'homeScreen_title_available',
      desc: '',
      args: [],
    );
  }

  /// `Selected worlds`
  String get homeScreen_title_selected {
    return Intl.message(
      'Selected worlds',
      name: 'homeScreen_title_selected',
      desc: '',
      args: [],
    );
  }

  /// `Created worlds`
  String get homeScreen_title_created {
    return Intl.message(
      'Created worlds',
      name: 'homeScreen_title_created',
      desc: '',
      args: [],
    );
  }

  /// `Dark theme`
  String get homeScreen_drawer_darkTheme {
    return Intl.message(
      'Dark theme',
      name: 'homeScreen_drawer_darkTheme',
      desc: '',
      args: [],
    );
  }

  /// `Go to profile`
  String get homeScreen_drawer_userCard_button_auth {
    return Intl.message(
      'Go to profile',
      name: 'homeScreen_drawer_userCard_button_auth',
      desc: '',
      args: [],
    );
  }

  /// `Sign in`
  String get homeScreen_drawer_userCard_button_unauth {
    return Intl.message(
      'Sign in',
      name: 'homeScreen_drawer_userCard_button_unauth',
      desc: '',
      args: [],
    );
  }

  /// `User is missing`
  String get homeScreen_drawer_userCard_title_default {
    return Intl.message(
      'User is missing',
      name: 'homeScreen_drawer_userCard_title_default',
      desc: '',
      args: [],
    );
  }

  /// `Id:`
  String get worldScreen_card_id {
    return Intl.message(
      'Id:',
      name: 'worldScreen_card_id',
      desc: '',
      args: [],
    );
  }

  /// `Type:`
  String get worldScreen_card_type {
    return Intl.message(
      'Type:',
      name: 'worldScreen_card_type',
      desc: '',
      args: [],
    );
  }

  /// `Title:`
  String get worldScreen_card_title {
    return Intl.message(
      'Title:',
      name: 'worldScreen_card_title',
      desc: '',
      args: [],
    );
  }

  /// `Size:`
  String get worldScreen_card_size {
    return Intl.message(
      'Size:',
      name: 'worldScreen_card_size',
      desc: '',
      args: [],
    );
  }

  /// `Description:`
  String get worldScreen_card_description {
    return Intl.message(
      'Description:',
      name: 'worldScreen_card_description',
      desc: '',
      args: [],
    );
  }

  /// `Owner:`
  String get worldScreen_card_owner {
    return Intl.message(
      'Owner:',
      name: 'worldScreen_card_owner',
      desc: '',
      args: [],
    );
  }

  /// `Open`
  String get worldScreen_button_open {
    return Intl.message(
      'Open',
      name: 'worldScreen_button_open',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get searchWorldsScreen_title {
    return Intl.message(
      'Search',
      name: 'searchWorldsScreen_title',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get searchWorldsScreen_label_name {
    return Intl.message(
      'Title',
      name: 'searchWorldsScreen_label_name',
      desc: '',
      args: [],
    );
  }

  /// `Author`
  String get searchWorldsScreen_label_authorName {
    return Intl.message(
      'Author',
      name: 'searchWorldsScreen_label_authorName',
      desc: '',
      args: [],
    );
  }

  /// `Author Id`
  String get searchWorldsScreen_label_authorId {
    return Intl.message(
      'Author Id',
      name: 'searchWorldsScreen_label_authorId',
      desc: '',
      args: [],
    );
  }

  /// `Search`
  String get searchWorldsScreen_button_search {
    return Intl.message(
      'Search',
      name: 'searchWorldsScreen_button_search',
      desc: '',
      args: [],
    );
  }

  /// `Log In`
  String get loginScreen_title_logIn {
    return Intl.message(
      'Log In',
      name: 'loginScreen_title_logIn',
      desc: '',
      args: [],
    );
  }

  /// `Sign Up`
  String get loginScreen_title_signUp {
    return Intl.message(
      'Sign Up',
      name: 'loginScreen_title_signUp',
      desc: '',
      args: [],
    );
  }

  /// `Already Registered? Log In Here`
  String get loginScreen_link_logIn {
    return Intl.message(
      'Already Registered? Log In Here',
      name: 'loginScreen_link_logIn',
      desc: '',
      args: [],
    );
  }

  /// `Not Registered? Sign Up Here`
  String get loginScreen_link_signUp {
    return Intl.message(
      'Not Registered? Sign Up Here',
      name: 'loginScreen_link_signUp',
      desc: '',
      args: [],
    );
  }

  /// `LogIn`
  String get loginScreen_button_logIn {
    return Intl.message(
      'LogIn',
      name: 'loginScreen_button_logIn',
      desc: '',
      args: [],
    );
  }

  /// `Register`
  String get loginScreen_button_signUp {
    return Intl.message(
      'Register',
      name: 'loginScreen_button_signUp',
      desc: '',
      args: [],
    );
  }

  /// `Username`
  String get loginScreen_input_username {
    return Intl.message(
      'Username',
      name: 'loginScreen_input_username',
      desc: '',
      args: [],
    );
  }

  /// `E-Mail`
  String get loginScreen_input_email {
    return Intl.message(
      'E-Mail',
      name: 'loginScreen_input_email',
      desc: '',
      args: [],
    );
  }

  /// `Password`
  String get loginScreen_input_password {
    return Intl.message(
      'Password',
      name: 'loginScreen_input_password',
      desc: '',
      args: [],
    );
  }

  /// `Verify password`
  String get loginScreen_input_repeatPassword {
    return Intl.message(
      'Verify password',
      name: 'loginScreen_input_repeatPassword',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an username`
  String get loginScreen_input_username_validate_fail {
    return Intl.message(
      'Please enter an username',
      name: 'loginScreen_input_username_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Username exists`
  String get loginScreen_input_username_exists {
    return Intl.message(
      'Username exists',
      name: 'loginScreen_input_username_exists',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an Email address`
  String get loginScreen_input_email_validate_fail {
    return Intl.message(
      'Please enter an Email address',
      name: 'loginScreen_input_email_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Email exists`
  String get loginScreen_input_email_exists {
    return Intl.message(
      'Email exists',
      name: 'loginScreen_input_email_exists',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a password`
  String get loginScreen_input_password_validate_fail {
    return Intl.message(
      'Please enter a password',
      name: 'loginScreen_input_password_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Please repeat your password`
  String get loginScreen_input_repeatPassword_validate_fail {
    return Intl.message(
      'Please repeat your password',
      name: 'loginScreen_input_repeatPassword_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Current user`
  String get currentUserScreen_title {
    return Intl.message(
      'Current user',
      name: 'currentUserScreen_title',
      desc: '',
      args: [],
    );
  }

  /// `Logout`
  String get currentUserScreen_button_logout {
    return Intl.message(
      'Logout',
      name: 'currentUserScreen_button_logout',
      desc: '',
      args: [],
    );
  }

  /// `Id:`
  String get userWidget_card_id {
    return Intl.message(
      'Id:',
      name: 'userWidget_card_id',
      desc: '',
      args: [],
    );
  }

  /// `Username:`
  String get userWidget_card_username {
    return Intl.message(
      'Username:',
      name: 'userWidget_card_username',
      desc: '',
      args: [],
    );
  }

  /// `E-Mail:`
  String get userWidget_card_email {
    return Intl.message(
      'E-Mail:',
      name: 'userWidget_card_email',
      desc: '',
      args: [],
    );
  }

  /// `Created worlds: {count}`
  String userWidget_card_worldsCreated(Object count) {
    return Intl.message(
      'Created worlds: $count',
      name: 'userWidget_card_worldsCreated',
      desc: '',
      args: [count],
    );
  }

  /// `Показать`
  String get userWidget_card_worldsCreated_button {
    return Intl.message(
      'Показать',
      name: 'userWidget_card_worldsCreated_button',
      desc: '',
      args: [],
    );
  }

  /// `Creating a world`
  String get createWorldScreen_title {
    return Intl.message(
      'Creating a world',
      name: 'createWorldScreen_title',
      desc: '',
      args: [],
    );
  }

  /// `Create`
  String get createWorldScreen_button_create {
    return Intl.message(
      'Create',
      name: 'createWorldScreen_button_create',
      desc: '',
      args: [],
    );
  }

  /// `Title`
  String get createWorldScreen_input_title {
    return Intl.message(
      'Title',
      name: 'createWorldScreen_input_title',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an title`
  String get createWorldScreen_input_title_validate_fail {
    return Intl.message(
      'Please enter an title',
      name: 'createWorldScreen_input_title_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Type`
  String get createWorldScreen_input_type {
    return Intl.message(
      'Type',
      name: 'createWorldScreen_input_type',
      desc: '',
      args: [],
    );
  }

  /// `Width`
  String get createWorldScreen_input_width {
    return Intl.message(
      'Width',
      name: 'createWorldScreen_input_width',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an width`
  String get createWorldScreen_input_width_validate_fail_0 {
    return Intl.message(
      'Please enter an width',
      name: 'createWorldScreen_input_width_validate_fail_0',
      desc: '',
      args: [],
    );
  }

  /// ` > 0`
  String get createWorldScreen_input_width_validate_fail_1 {
    return Intl.message(
      ' > 0',
      name: 'createWorldScreen_input_width_validate_fail_1',
      desc: '',
      args: [],
    );
  }

  /// `Height`
  String get createWorldScreen_input_height {
    return Intl.message(
      'Height',
      name: 'createWorldScreen_input_height',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an height`
  String get createWorldScreen_input_height_validate_fail_0 {
    return Intl.message(
      'Please enter an height',
      name: 'createWorldScreen_input_height_validate_fail_0',
      desc: '',
      args: [],
    );
  }

  /// ` > 0`
  String get createWorldScreen_input_height_validate_fail_1 {
    return Intl.message(
      ' > 0',
      name: 'createWorldScreen_input_height_validate_fail_1',
      desc: '',
      args: [],
    );
  }

  /// `Description`
  String get createWorldScreen_input_description {
    return Intl.message(
      'Description',
      name: 'createWorldScreen_input_description',
      desc: '',
      args: [],
    );
  }

  /// `Please enter an description`
  String get createWorldScreen_input_description_validate_fail {
    return Intl.message(
      'Please enter an description',
      name: 'createWorldScreen_input_description_validate_fail',
      desc: '',
      args: [],
    );
  }

  /// `Loading`
  String get reflectionScreen_title_loading {
    return Intl.message(
      'Loading',
      name: 'reflectionScreen_title_loading',
      desc: '',
      args: [],
    );
  }

  /// `Browsing`
  String get reflectionScreen_title_browsing {
    return Intl.message(
      'Browsing',
      name: 'reflectionScreen_title_browsing',
      desc: '',
      args: [],
    );
  }

  /// `Editing`
  String get reflectionScreen_title_editing {
    return Intl.message(
      'Editing',
      name: 'reflectionScreen_title_editing',
      desc: '',
      args: [],
    );
  }

  /// `Download failed`
  String get reflectionScreen_card_loadingFail {
    return Intl.message(
      'Download failed',
      name: 'reflectionScreen_card_loadingFail',
      desc: '',
      args: [],
    );
  }

  /// `Options`
  String get reflectionScreen_button_options {
    return Intl.message(
      'Options',
      name: 'reflectionScreen_button_options',
      desc: '',
      args: [],
    );
  }

  /// `Play`
  String get reflectionScreen_button_play {
    return Intl.message(
      'Play',
      name: 'reflectionScreen_button_play',
      desc: '',
      args: [],
    );
  }

  /// `Stop`
  String get reflectionScreen_button_stop {
    return Intl.message(
      'Stop',
      name: 'reflectionScreen_button_stop',
      desc: '',
      args: [],
    );
  }

  /// `Background`
  String get reflectionScreen_panel_browsing_item_background {
    return Intl.message(
      'Background',
      name: 'reflectionScreen_panel_browsing_item_background',
      desc: '',
      args: [],
    );
  }

  /// `Grid`
  String get reflectionScreen_panel_browsing_item_grid {
    return Intl.message(
      'Grid',
      name: 'reflectionScreen_panel_browsing_item_grid',
      desc: '',
      args: [],
    );
  }

  /// `Cell`
  String get reflectionScreen_panel_browsing_item_color_0_gof {
    return Intl.message(
      'Cell',
      name: 'reflectionScreen_panel_browsing_item_color_0_gof',
      desc: '',
      args: [],
    );
  }

  /// `Reset`
  String get reflectionScreen_panel_browsing_item_reset {
    return Intl.message(
      'Reset',
      name: 'reflectionScreen_panel_browsing_item_reset',
      desc: '',
      args: [],
    );
  }

  /// `Remove`
  String get reflectionScreen_panel_editing_item_remove {
    return Intl.message(
      'Remove',
      name: 'reflectionScreen_panel_editing_item_remove',
      desc: '',
      args: [],
    );
  }

  /// `Cell`
  String get reflectionScreen_panel_editing_item_pain_0_gof {
    return Intl.message(
      'Cell',
      name: 'reflectionScreen_panel_editing_item_pain_0_gof',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}
import 'package:koin_flutter/koin_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wonderful_world/data/model/login/login_body.dart';
import 'package:wonderful_world/data/model/register_body/register_body.dart';
import 'package:wonderful_world/data/model/user/user.dart';
import 'package:wonderful_world/data/repository/network/user_repository/user_repository.dart';

class UserCenter implements Disposable {
  final UserRepository _userRepository;
  bool _isAuth;

  final PublishSubject<bool> _authStateChangeObservable =
      PublishSubject<bool>();
  PublishSubject<bool> get authStateChangeObservable =>
      _authStateChangeObservable;
  void _authStateChange(bool isAuth) {
    _isAuth = isAuth;
    _authStateChangeObservable.add(isAuth);
  }

  UserCenter(this._userRepository);

  Future<bool> isAuth({bool isUpgrade = false}) async {
    if (isUpgrade || _isAuth == null) {
      _authStateChange(await _userRepository.check());
      _isAuth ??= false;
    }
    return _isAuth;
  }

  Future<User> current() async {
    return await _userRepository.current();
  }

  Future<User> user(String id) async {
    return await _userRepository.user(id);
  }

  Future<bool> register(String username, String email, String password) async {
    var result = await _userRepository.register(
      RegisterBody(username, email, password),
    );
    _authStateChange(result);
    return result;
  }

  Future<bool> login(String email, String password) async {
    var result = await _userRepository.login(LoginBody(email, password));
    _authStateChange(result);
    return result;
  }

  Future<bool> loginVk(String code) async {
    var result = await _userRepository.loginVk(code);
    _authStateChange(result);
    return result;
  }

  Future<bool> logout() async {
    var result = await _userRepository.logout();
    _authStateChange(false);
    return result;
  }

  Future<bool> existsUsername(String username) async {
    var result = await _userRepository.existsUsername(username);
    return result;
  }

  Future<bool> existsEmail(String email) async {
    var result = await _userRepository.existsEmail(email);
    return result;
  }

  @override
  void dispose() {
    _authStateChangeObservable.close();
  }
}

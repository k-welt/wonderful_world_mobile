import 'app_command.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';

class EmptyCommand implements AppCommand {
  const EmptyCommand();
  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    return IsolateCommand.empty;
  }
}

import '../model/reflect_item.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';
import 'empty_command.dart';
import 'loadingfailed_command.dart';
import 'reflectionshot_command.dart';
import 'savecompleted_command.dart';
import 'savefailed_command.dart';
import 'stopped_command.dart';
import 'unauthorized_command.dart';
import 'loadingcompleted_command.dart';
import 'jiffmillisecduration_command.dart';

abstract class AppCommand {
  Future<IsolateCommand> exec(WarpReflectionCenter receiver);

  static AppCommand get empty => const EmptyCommand();

  factory AppCommand.loadingCompleted() {
    return LoadingCompletedCommand();
  }

  factory AppCommand.loadingFailed() {
    return LoadingFailedCommand();
  }

  factory AppCommand.saveCompleted() {
    return SaveCompletedCommand();
  }

  factory AppCommand.saveFailed() {
    return SaveFailedCommand();
  }

  factory AppCommand.unauthorized(IsolateCommand parentCommand) {
    return UnauthorizedCommand(parentCommand);
  }

  factory AppCommand.reflectionShot(List<ReflectItem> items) {
    return ReflectionshotCommand(items);
  }

  factory AppCommand.stopped() {
    return StoppedCommand();
  }

  factory AppCommand.jiffmillisecdurationCommand(int millisec) {
    return JiffmillisecdurationCommand(millisec);
  }
}

extension AppCommandEmptyTester on AppCommand {
  bool get isEmpty => this is EmptyCommand;
  bool get isNotEmpty => !isEmpty;
}

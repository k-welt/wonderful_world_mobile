import 'app_command.dart';
import '../warpreflection_center.dart';
import '../model/reflect_item.dart';
import '../isolate/command/isolate_command.dart';

class ReflectionshotCommand implements AppCommand {
  final List<ReflectItem> items;

  const ReflectionshotCommand(this.items);

  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    receiver.reflectionObservable.add(items);
    return IsolateCommand.empty;
  }
}

import 'app_command.dart';
import '../warpreflection_center.dart';
import '../isolate/command/isolate_command.dart';

class SaveCompletedCommand implements AppCommand {
  @override
  Future<IsolateCommand> exec(WarpReflectionCenter receiver) async {
    return IsolateCommand.empty;
  }
}
import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class LoadingCommand implements IsolateCommand {
  final String id;

  const LoadingCommand(this.id);

  @override
  Future<AppCommand> exec(WarpController receiver) async {
    try {
      await receiver.loading(id);
      return AppCommand.loadingCompleted();
    } catch (ex) {
      return AppCommand.loadingFailed();
    }
  }
}

import 'dart:math';

import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class InsertCommand implements IsolateCommand {
  final Point<int> point;

  const InsertCommand(this.point);

  @override
  Future<AppCommand> exec(WarpController receiver) async {
    receiver.insert(point);
    return AppCommand.empty;
  }
}

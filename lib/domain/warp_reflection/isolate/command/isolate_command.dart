import 'dart:math';

import 'package:wonderful_world/domain/warp_reflection/isolate/command/getjiffmillisecduration_command.dart';
import 'package:wonderful_world/domain/warp_reflection/isolate/command/lastdata_loading_command.dart';

import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'clear_command.dart';
import 'empty_comand.dart';
import 'exit_command.dart';
import 'getreflection_command.dart';
import 'loading_command.dart';
import 'insert_command.dart';
import 'remove_command.dart';
import 'save_command.dart';
import 'setjiffmillisecduration_command.dart';
import 'settoken_command.dart';
import 'start_command.dart';
import 'stop_command.dart';

abstract class IsolateCommand {
  Future<AppCommand> exec(WarpController receiver);

  static IsolateCommand get empty => const EmptyCommand();

  factory IsolateCommand.clear() {
    return ClearCommand();
  }

  factory IsolateCommand.exit() {
    return ExitCommand();
  }

  factory IsolateCommand.getReflection() {
    return GetReflectionCommand();
  }

  factory IsolateCommand.loading(String id) {
    return LoadingCommand(id);
  }

  factory IsolateCommand.lastDataLoading() {
    return LastDataLoadingCommand();
  }

  factory IsolateCommand.save() {
    return SaveCommand();
  }

  factory IsolateCommand.setJiffMilliSecDuration(int milliseconds) {
    return SetJiffMilliSecDurationCommand(milliseconds);
  }

  factory IsolateCommand.setToken(String token, IsolateCommand parentCommand) {
    return SetTokenCommand(token, parentCommand);
  }

  factory IsolateCommand.start() {
    return StartCommand();
  }

  factory IsolateCommand.stop() {
    return StopCommand();
  }

  factory IsolateCommand.insert(Point<int> point) {
    return InsertCommand(point);
  }

  factory IsolateCommand.remove(Point<int> point) {
    return RemoveCommand(point);
  }

  factory IsolateCommand.getJiffMilliSecDuration() {
    return GetJiffMilliSecDurationCommand();
  }
}

extension IsolateCommandEmptyTester on IsolateCommand {
  bool get isEmpty => this is EmptyCommand;
  bool get isNotEmpty => !isEmpty;
}

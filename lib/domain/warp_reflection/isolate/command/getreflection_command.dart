import '../../command/app_command.dart';
import '../controller/warp_controller.dart';

import 'isolate_command.dart';

class GetReflectionCommand implements IsolateCommand {
  @override
  Future<AppCommand> exec(WarpController receiver) async {
    return AppCommand.reflectionShot(receiver.getReflection());
  }
}

import 'dart:math';

import 'package:wonderful_world/data/model/world/world.dart';
import 'package:wonderful_world/data/model/world_data/world_data.dart';
import 'package:wonderful_world/domain/warp_reflection/isolate/controller/runner/gol_runner.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';

abstract class WarpRunner {
  List<ReflectItem> getReflection();
  WorldComponents getData();

  bool jiff();
  void insert(Point<int> point);
  void remove(Point<int> point);

  factory WarpRunner.fromData(WorldData data) {
    if (data.type == WorldTypes.kGOLb2s23MValue ||
        data.type == WorldTypes.kGOLb3s012345678MValue ||
        data.type == WorldTypes.kGOLb1s012345678MValue ||
        data.type == WorldTypes.kGOLb1s012345678NValue ||
        data.type == WorldTypes.kGOLb3678s34678MValue) {
      var bs = WorldTypes.kBSValues[data.type];
      return GOLRunner(data, bs.b, bs.s, bs.isMoore);
    } else {
      return null;
    }
  }
}

import 'dart:math';

import 'package:wonderful_world/data/model/world_data/world_data.dart';
import 'package:wonderful_world/domain/warp_reflection/model/gol_reflectitem.dart.dart';
import 'package:wonderful_world/domain/warp_reflection/model/reflect_item.dart';

import 'warp_runner.dart';

class GOLRunner implements WarpRunner {
  final NaturalSize _size;
  final List<int> _b;
  final List<int> _s;
  final bool isMoore;
  List<List<bool>> _cells;

  GOLRunner._(this._cells, this._b, this._s, this.isMoore, this._size);
  factory GOLRunner(WorldData data, List<int> b, List<int> s, bool isMoore) {
    var result;
    var cells = <List<bool>>[
      for (int i = 0; i < data.size.width; ++i)
        List<bool>.filled(data.size.height, false)
    ];

    for (var iter in data.data.cells0) {
      cells[iter.x][iter.y] = true;
    }

    result = GOLRunner._(cells, b, s, isMoore, data.size);

    return result;
  }

  @override
  WorldComponents getData() {
    return WorldComponents(<CellT0>[
      for (int i = 0; i < _size.width; ++i)
        for (int j = 0; j < _size.height; ++j)
          if (_cells[i][j]) CellT0(i, j)
    ]);
  }

  @override
  List<ReflectItem> getReflection() {
    return <ReflectItem>[
      for (int i = 0; i < _size.width; ++i)
        for (int j = 0; j < _size.height; ++j)
          if (_cells[i][j]) GOLReflectItem(i, j)
    ];
  }

  @override
  void insert(Point<int> point) {
    _cells[point.x][point.y] = true;
  }

  @override
  void remove(Point<int> point) {
    _cells[point.x][point.y] = false;
  }

  @override
  bool jiff() {
    var isChanged = false;
    var newGeneration = List<List<bool>>(_size.width);
    for (int i = 0; i < _size.width; ++i) {
      newGeneration[i] = List<bool>(_size.height);
    }
    var neighbors = List<Point<int>>((isMoore) ? 8 : 4);

    bool bufferOldValue;
    bool bufferValue;
    for (int i = 0; i < _size.width; ++i) {
      for (int j = 0; j < _size.height; ++j) {
        var count = _countLivingNeighbors(_getNeighbors(neighbors, i, j));
        bufferOldValue = _cells[i][j];
        bufferValue =
            _b.contains(count) || (bufferOldValue && _s.contains(count));
        isChanged = isChanged || bufferOldValue != bufferValue;
        newGeneration[i][j] = bufferValue;
      }
    }
    _cells = newGeneration;

    return isChanged;
  }

  int _countLivingNeighbors(List<Point<int>> neighbors) {
    int result = 0;

    for (Point iter in neighbors) if (_cells[iter.x][iter.y]) ++result;

    return result;
  }

  List<Point<int>> _getNeighbors(List<Point<int>> neighbors, int x, int y) {
    neighbors[0] = Point(x, _getY(y - 1));
    neighbors[1] = Point(_getX(x + 1), y);
    neighbors[2] = Point(x, _getY(y + 1));
    neighbors[3] = Point(_getX(x - 1), y);

    if (isMoore) {
      neighbors[4] = Point(_getX(x + 1), _getY(y - 1));
      neighbors[5] = Point(_getX(x + 1), _getY(y + 1));
      neighbors[6] = Point(_getX(x - 1), _getY(y + 1));
      neighbors[7] = Point(_getX(x - 1), _getY(y - 1));
    }

    return neighbors;
  }

  int _getX(int x) {
    if (x < 0) {
      return _size.width - (x.abs() % _size.width);
    } else {
      if (x < _size.width) {
        return x;
      } else {
        return x % _size.width;
      }
    }
  }

  int _getY(int y) {
    if (y < 0) {
      return _size.height - (y.abs() % _size.height);
    } else {
      if (y < _size.height) {
        return y;
      } else {
        return y % _size.height;
      }
    }
  }
}

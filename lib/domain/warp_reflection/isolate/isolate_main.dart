import 'dart:isolate';

import 'package:koin/koin.dart';
import 'package:wonderful_world/data/data_source/local/db/di_module.dart';
import 'package:wonderful_world/data/di_module.dart';
import 'package:wonderful_world/domain/warp_reflection/isolate/controller/di_module.dart';

import 'warp_isolate.dart';

Future<void> isolateMain(SendPort sendPort) async {
  var receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);

  String path = await receivePort.first;
  var koin = startKoin((app) {
    app.modules(<Module>[
      getDbStoreModule(path),
      ...worldDataDataModules,
      warpControllerModule,
    ]);
  });
  
  receivePort = ReceivePort();
  sendPort.send(receivePort.sendPort);

  var warp = WarpIsolate(sendPort, receivePort);
  await warp.commandLoop();
  await warp.dispose();
  koin.close();
}
